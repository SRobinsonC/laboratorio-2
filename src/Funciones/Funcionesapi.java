package Funciones;

public class Funcionesapi {
    public static void main(String[] args) {

        double sin = Math.sin(50);
        double cos = Math.cos(50);
        double tan = Math.tan(50);
        double atan = Math.atan(50);
        double atan2 = Math.atan2(2,5);
        double exp = Math.exp(5);
        double log = Math.log(5);
        double pi = Math.PI+1;
        double e = Math.E+1;

        System.out.println("Funciones Trigonométricas");
        System.out.println("sin =" +sin);
        System.out.println("cos =" +cos);
        System.out.println("tan = "+tan);
        System.out.println("atan = "+atan);
        System.out.println("atan2 = "+atan2);

        System.out.println("Otras Funciones");
        System.out.println("exp = "+exp);
        System.out.println("log = "+log);
        System.out.println("PI = "+pi);
        System.out.println("E = "+e);

    }
}
