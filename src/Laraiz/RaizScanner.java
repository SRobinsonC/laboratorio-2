package Laraiz;


import java.util.Scanner;

public class RaizScanner {


    public static void main (String [] args  ){

        Scanner entrada = new Scanner(System.in);


        System.out.println("Ingrese un numero :");
        double num = entrada.nextDouble();
        double resultado = Math.sqrt(num);
        System.out.println("La raiz cuadrada de "  + num +  " es " +resultado);

    }
}
