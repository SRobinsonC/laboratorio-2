package TrabajoIndividualEditordeTexto;

import javax.swing.*;
import javax.swing.text.StyledEditorKit;
import java.awt.*;
import java.awt.event.*;
import java.io.*;


public class EditordeTexto {
    public static void main(String[] arg) {
        Cuadro marco = new Cuadro();

}
}

class Cuadro extends JFrame {
    public Cuadro(){
        Toolkit pantalla = Toolkit.getDefaultToolkit();
        Dimension tamanoPantalla = pantalla.getScreenSize();
        int alturaPantalla = tamanoPantalla.height;
        int anchoPantala = tamanoPantalla.width;
        setBounds(anchoPantala/4,alturaPantalla/4,anchoPantala/2,alturaPantalla/2);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Procesador de Texto");
        Panel miPanel = new Panel();
        add(miPanel);
        addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {

                JOptionPane.showMessageDialog(null, " Se guardara el trabajo realizado y despues se cerrara el programa");

                miPanel.guardarArchivos();

            }

        });




    }


}



class Panel extends JPanel {

    private static JTextPane textArea;

    JMenu archivo, fuente, estilo, tamanio;

    Font letra;


    public Panel(){
        setLayout(new BorderLayout());
        JPanel menu = new JPanel();

        JMenuBar miBarra = new JMenuBar();

        archivo = new JMenu("Archivo");
        fuente = new JMenu("Fuente");
        estilo = new JMenu("Estilo");
        tamanio = new JMenu("Tamaño");

        configuraMenu("Abrir", "archivo","",9,10);
        configuraMenu("Guardar", "archivo","",9,10);

        configuraMenu("Arial", "fuente","Arial",9,10);
        configuraMenu("Times New Roman", "fuente","Times New Roman",9,10);
        configuraMenu("Verdana", "fuente","Verdana",9,10);

        configuraMenu("Negrita", "estilo","",Font.BOLD,10);
        configuraMenu("Cursiva", "estilo","",Font.ITALIC,10);

        configuraMenu("10", "tamanio","",0,10);
        configuraMenu("12", "tamanio","",0,12);
        configuraMenu("14", "tamanio","",0,14);
        configuraMenu("16", "tamanio","",0,16);

        miBarra.add(archivo);
        miBarra.add(fuente);
        miBarra.add(estilo);
        miBarra.add(tamanio);



        menu.add(miBarra);
        add(menu,BorderLayout.NORTH);
        textArea = new JTextPane();

        add(textArea, BorderLayout.CENTER);



    }
    public void configuraMenu( String rotulo, String menus, String tipo_letra, int estilos, int tam){
        JMenuItem elem_menu = new JMenuItem(rotulo);
        if(menus == "archivo") {
            archivo.add(elem_menu);
            if (rotulo == "Abrir"){
                elem_menu.addActionListener(new abriendoArchivo());
            }else if (rotulo == "Guardar"){
                elem_menu.addActionListener(new guardandoArchivo());
            }
        }else if (menus == "fuente"){
            fuente.add(elem_menu);
            if (tipo_letra == "Arial"){
                elem_menu.addActionListener(new StyledEditorKit.FontFamilyAction("cambiar letra","Arial"));
            }else if (tipo_letra == "Times New Roman"){
                elem_menu.addActionListener(new StyledEditorKit.FontFamilyAction("cambiar letra","Times New Roman"));
            }else if (tipo_letra == "Verdana"){
                elem_menu.addActionListener(new StyledEditorKit.FontFamilyAction("cambiar letra","Verdana"));
            }

        }else if (menus == "estilo"){
            estilo.add(elem_menu);
            if (estilos == Font.BOLD){
                elem_menu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK));
                elem_menu.addActionListener(new StyledEditorKit.BoldAction());
            }else if (estilos == Font.ITALIC){
                elem_menu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K, InputEvent.CTRL_DOWN_MASK));
                elem_menu.addActionListener(new StyledEditorKit.ItalicAction());
            }
        }
        else if( menus == "tamanio"){
            tamanio.add(elem_menu);
            elem_menu.addActionListener(new StyledEditorKit.FontSizeAction("",tam));
        }


    }

    public static void abrirArchivo() {

        JFileChooser seleccionador=new JFileChooser();

        int archivoSeleccionado= seleccionador.showOpenDialog(textArea);

        if(archivoSeleccionado==JFileChooser.APPROVE_OPTION) {

            File fichero= seleccionador.getSelectedFile();

            fichero.getAbsolutePath();

            try(FileReader lectorArchivos=new FileReader(fichero)){

                String cadena="";

                int valor=lectorArchivos.read();

                while(valor!=-1){

                    cadena=cadena+(char)valor;

                    valor=lectorArchivos.read();

                }

                textArea.setText(cadena);

            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block

                e.printStackTrace();

                JOptionPane.showMessageDialog(null, "No se ha encontro el archivo");

            } catch (IOException e) {
                // TODO Auto-generated catch block

                e.printStackTrace();

                JOptionPane.showMessageDialog(null, " I/O ERROR");

            }

        }

    }

    class abriendoArchivo implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0) {
            // TODO Auto-generated method stub

            Panel.abrirArchivo();

        }

    }



    public static void guardarArchivos() {

        try {

            ObjectOutputStream saveArch = new ObjectOutputStream(new FileOutputStream(

                    "C:\\Users\\54362\\Desktop\\REPOSITORIO LAB2\\laboratorio-2\\src\\TrabajoIndividualEditordeTexto\\documento.txt"));

            saveArch.writeObject(textArea.getText());

            saveArch.close();

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block

            e.printStackTrace();

            JOptionPane.showMessageDialog(null, "No encontro el archivo");

        } catch (IOException e) {
            // TODO Auto-generated catch block

            e.printStackTrace();

            JOptionPane.showMessageDialog(null, " I/O ERROR");

        }

    }
    private class guardandoArchivo implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            // TODO Auto-generated method stub

            Panel.guardarArchivos();

        }

    }

    private class GestionaEventos implements ActionListener{

        String tipo_texto, menu;
        int estilo_letra, tamanio_letra;
        GestionaEventos(String elemento, String texto2, int estilo2, int tam_letra){
            tipo_texto = texto2;
            estilo_letra = estilo2;
            tamanio_letra = tam_letra;
            menu = elemento;
        }
        @Override
        public void actionPerformed(ActionEvent e) {
            letra = textArea.getFont();
            if (menu == "Arial" || menu == "Times New Roman" || menu == "Verdana"){
                estilo_letra = letra.getStyle();
                tamanio_letra = letra.getSize();
            }else if (menu == "Cursiva" || menu == "Negrita" ){
                if (letra.getStyle() == 1 || letra.getStyle() == 2){
                    estilo_letra = 3;
                }
                tipo_texto = letra.getFontName();
                tamanio_letra = letra.getSize();
            }else if (menu == "10" || menu == "12" || menu == "14" || menu == "16"){
                estilo_letra = letra.getStyle();
                tipo_texto = letra.getFontName();
            }
            textArea.setFont(new Font(tipo_texto,estilo_letra, tamanio_letra));
        }
    }
}
