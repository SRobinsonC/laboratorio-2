package desafio10;

import java.util.Scanner;

public class Adivinanza {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);

        int aleatorio = (int) (Math.random()*100+1);
        System.out.println("Bienvenido, tienes que adivinar un numero entre el 1 al 100\n");

        int numero;
        int cont =0;
        do {
            System.out.println("ingresa un numero ");
            numero = entrada.nextInt();
            cont++;
            if (numero<aleatorio)
                System.out.println("intenta con un numero mayor");
            else if (numero>aleatorio)
                System.out.println("intenta con un numero menor");

        } while (numero !=aleatorio);

          System.out.println("Correcto adivinaste !!\n El numero es : " +numero + " \n intentos = " + cont);



    }

}
