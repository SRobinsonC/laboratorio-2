package DesafioCalculadora;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Calculadora {

    public static void main(String[] arg) {
        Principal pantallaPrincipal = new Principal();

        pantallaPrincipal.setVisible(true);

}


    static class Cuadro extends JPanel{
        private static final long serialVersionUID = 1L;

        private JButton txt =new JButton("0");
        private JPanel numeros;
        private boolean comienzo=true;
        private String calculo;
        private double total;


        public Cuadro() {
            setLayout(new BorderLayout());
            txt.setEnabled(false);
            add(txt, BorderLayout.NORTH);
            numeros = new JPanel();
            numeros.setLayout(new GridLayout(4,4));
            pornerCeroPantalla limpiador = new pornerCeroPantalla();
            ingresarNumeros escuchador = new ingresarNumeros();
            Operadores operador = new Operadores();


            //Primer fila
            crearBotones("7", escuchador);
            crearBotones("8", escuchador);
            crearBotones("9", escuchador);
            crearBotones("x", operador);

            //Segunda fila
            crearBotones("4", escuchador);
            crearBotones("5", escuchador);
            crearBotones("6", escuchador);
            crearBotones("-", operador);

            //Tercer fila
            crearBotones("1",escuchador);
            crearBotones("2",escuchador);
            crearBotones("3",escuchador);
            crearBotones("+", operador);

            //Cuarta fila
            crearBotones("/", operador);
            crearBotones("0",escuchador);
            crearBotones("AC", limpiador);
            crearBotones("=", operador);

            add(numeros);
            calculo="=";

        }


        private class ingresarNumeros implements ActionListener {

            @Override
            public void actionPerformed(ActionEvent e) {
                String numero = e.getActionCommand();
                if(comienzo){
                    txt.setText("");
                    comienzo=false;

                }
                txt.setText(txt.getText()+numero);
            }
        }

        private class Operadores implements ActionListener{

            @Override
            public void actionPerformed(ActionEvent e) {
                String ingreso = e.getActionCommand();
                calcular(Double.parseDouble(txt.getText()));
                calculo=ingreso;
                comienzo=true;

            }

            public void calcular(Double numero){
                try{
                    if (calculo.equals("+")){
                        total+=numero;
                    } else if (calculo.equals("-")){
                        total-=numero;
                    } else if (calculo.equals("x")){
                        total*=numero;
                    } else if (calculo.equals("/")){
                        total/=numero;
                    } else if (calculo.equals("=")){
                        total=numero;
                    } else if (calculo.equals("AC")){
                        total=0;
                    }
                    txt.setText(""+total);
                } catch (ArithmeticException e){
                    System.out.println("No se puede dividir por 0");
                }

            }
        }
        private class pornerCeroPantalla implements ActionListener{

            @Override
            public void actionPerformed(ActionEvent e) {
                txt.setText("0");
            }
        }

        private void crearBotones(String nombre, ActionListener escuchador){
            JButton btn = new JButton(nombre);
            btn.addActionListener(escuchador);
            numeros.add(btn);

        }




    }

    public static class Principal extends JFrame {


        private static final long serialVersionUID = 1L;

        public Principal() {

            setTitle("Trabajo Practico: La Calculadora;");

            Toolkit kit= Toolkit.getDefaultToolkit();

            Dimension screenSize=kit.getScreenSize();

            int height = screenSize.height;
            int width = screenSize.width;

            setSize(600,600);
            setLocation(width/4, height/6);

            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            Cuadro panel1= new Cuadro();
            add(panel1);


        }

    }
}





